const routes = [
  {
    path: '/',
    component: () => import('layouts/LayoutsUtama'),
    children: [
      { path: '', component: () => import('pages/Index.vue') },
      { path: '/google/:token', component: () => import('pages/GoogleAuth') },
      { path: 'c/:category', component: () => import('pages/Kategori/Front') },
      { path: 'c/m/:category', component: () => import('pages/Kategori/FrontMobile') },
      { path: 'ketentuan', component: () => import('pages/ketentuan.vue') },
      { path: 'message', component: () => import('pages/chat.vue') },
      { path: 'logout', component: () => import('pages/Logout.vue') },
      { path: 'detailProduct', component: () => import('pages/Product/AddNewProduct.vue') },
      { path: 'detailprodukjasa', component: () => import('pages/Admin/detailprodukjasa.vue') },
      { path: 'bergabungdalamproyek', component: () => import('pages/Admin/bergabungdalamproyek.vue') },
      { path: 'detailjasa', component: () => import('pages/Admin/detailjasa.vue') },
      { path: 'filterproduk', component: () => import('pages/Admin/filterproduk.vue') },
      { path: 'filterproduk-m', component: () => import('pages/Admin/filterproduk-m.vue') },
      { path: 'detailprodukbarang', component: () => import('pages/Admin/detailprodukbarang.vue') },
      { path: 'detailprodukbarang-m', component: () => import('pages/Admin/detailprodukbarang-m.vue') },
    ]
  },
  {
    path: '/',
    component: () => import('layouts/Login.vue'),
    children: [
      { path: 'login', component: () => import('pages/Authentication/Login.vue') },
      { path: 'register', component: () => import('pages/Authentication/Register.vue') },
      { path: 'verifikasiulang', component: () => import('pages/Authentication/verifikasiulang.vue') },
      { path: 'google', component: () => import('pages/Google') },
    ]
  },
  {
    path: '/',
    component: () => import('layouts/BackButton'),
    children: [
      { path: 'upgrade-level', component: () => import('pages/Level/UpgradeLevel') },
      { path: 'upgradelevel', component: () => import('pages/Admin/upgradelevel') },
      { path: 'metodepembayaran', component: () => import('pages/Admin/metodepembayaran.vue') },
    ]
  },
  {
    path: '/',
    component: () => import('layouts/AuthLayout.vue'),
    children: [
      { path: 'home', component: () => import('pages/Home.vue') },
      { path: 'profile', component: () => import('pages/Adaptive/Profile') },
      { path: 'provider', component: () => import('pages/Admin/Provider.vue') },
      { path: 'provider-m', component: () => import('pages/Admin/Provider-m.vue') },
      { path: 'produkjasa', component: () => import('pages/Admin/produkjasa.vue') },
      { path: 'listbanner', component: () => import('pages/Admin/Banner/ListBanner') },
      { path: '/publish/:id', component: () => import('pages/Product/Publish.vue') },
      { path: '/unpublish/:id', component: () => import('pages/Product/Unpublish.vue') },
      { path: 'inputketentuan', component: () => import('pages/Admin/inputketentuan.vue') },
      { path: 'konfirmasipembayaran', component: () => import('pages/Admin/konfirmasipembayaran.vue') },
      { path: 'konfirmasipembayaran-m', component: () => import('pages/Admin/konfirmasipembayaran-m.vue') },
    ]
  },
  {
    path: '/',
    component: () => import('layouts/LayoutPolos.vue'),
    children: [
      { path: 'chat', component: () => import('pages/Message.vue') },
      { path: 'chat-m', component: () => import('pages/Message-m.vue') },
      { path: 'chat/:id', component: () => import('pages/Message.vue') },
      { path: 'chat-m/:id', component: () => import('pages/Message.vue') },
    ]
  },
  {
    path: '/',
    component: () => import('layouts/AuthLayout.vue'),
    children: [
      { path: 'category', component: () => import('pages/Admin/Category.vue') },
      { path: 'tambahbu', component: () => import('pages/Admin/Tambahbu.vue') },
      { path: 'tambahjp', component: () => import('pages/Admin/Tambahjp.vue') },
      { path: 'tambahuser', component: () => import('pages/Admin/Tambahuser.vue') },
      { path: 'produksaya', component: () => import('pages/Admin/Produksaya.vue') },
      { path: 'daftarreferensikategori', component: () => import('pages/Admin/daftarreferensikategori.vue') },
      { path: 'daftarreferensibadanusaha', component: () => import('pages/Admin/daftarreferensibadanusaha.vue') },
      { path: 'daftarreferensijenisproduk', component: () => import('pages/Admin/daftarreferensijenisproduk.vue') },
      { path: 'daftaruser', component: () => import('pages/Admin/daftaruser.vue') },
      { path: 'listdatauser', component: () => import('pages/Admin/listdatauser.vue') },
      { path: 'tambahrole', component: () => import('pages/Admin/tambahrole.vue') },
      { path: 'tambahlevel', component: () => import('pages/Admin/tambahlevel.vue') },
      { path: 'detailapprovalproduk', component: () => import('pages/Admin/detailapprovalproduk.vue') },
      { path: 'tambahproduk', component: () => import('pages/Admin/Tambahproduk.vue') },
      { path: 'daftarreferensilevel', component: () => import('pages/Admin/daftarreferensilevel.vue') },
      { path: 'keranjangbelanja', component: () => import('pages/Admin/keranjangbelanja.vue') },
      { path: 'keranjangbelanja-m', component: () => import('pages/Admin/keranjangbelanja-m.vue') },
      { path: 'tambahbanner', component: () => import('pages/Admin/tambahbanner.vue') },
    ]
  },
  {
    path: '/',
    component: () => import('layouts/AuthLayout.vue'),
    children: [
      { path: 'faq', component: () => import('pages/informasi/faq.vue') },
      { path: 'daftarfaq', component: () => import('pages/informasi/daftarfaq.vue') },
      { path: 'inputfaq', component: () => import('pages/informasi/inputfaq.vue') },
    ]
  },
  {
    path: '/user',
    component: () => import('layouts/AuthLayout.vue'),
    children: [
      { path: '', component: () => import('pages/User/ListUser') },
      { path: 'role/:id', component: () => import('pages/User/RoleUser') },
    ]
  },
  {
    path: '/badan-usaha',
    component: () => import('layouts/AuthLayout.vue'),
    children: [
      { path: 'tambah', component: () => import('pages/BadanUsaha/Tambah.vue') },
      { path: '', component: () => import('pages/BadanUsaha/List.vue') },
      { path: 'hapus/:id', component: () => import('pages/BadanUsaha/Hapus.vue') },
      { path: 'edit/:id', component: () => import('pages/BadanUsaha/Edit.vue') },
    ]
  },
  {
    path: '/level',
    component: () => import('layouts/AuthLayout.vue'),
    children: [
      { path: '/', component: () => import('pages/Level/Index') },
      { path: 'tambah', component: () => import('pages/Level/Tambah') }
    ]
  },
  {
    path: '/product',
    component: () => import('layouts/LayoutsUtama.vue'),
    children: [
      { path: ':id', component: () => import('pages/Product/FrontDetail.vue') },
      { path: '-m/:id', component: () => import('pages/Product/FrontDetail-m.vue') },
      { path: 'hire/:id', component: () => import('pages/Product/HireDetail.vue') },
      { path: 'hire-m/:id', component: () => import('pages/Product/HireDetail-m.vue') },
    ]
  },
  {
    path: '/banner',
    component: () => import('layouts/AuthLayout'),
    children: [
      { path: '', component: () => import('pages/Admin/Banner/ListBanner') },
      { path: 'tambah', component: () => import('pages/Admin/Banner/TambahBanner') },
      { path: ':id/ubah', component: () => import('pages/Admin/Banner/UbahBanner') },
      { path: ':id/delete', component: () => import('pages/Admin/Banner/DeleteBanner') }
    ]
  },
  {
    path: '/iklan',
    component: () => import('layouts/AuthLayout'),
    children: [
      { path: '', component: () => import('pages/Admin/iklan/ListIklan') },
      { path: 'tambah', component: () => import('pages/Admin/iklan/TambahIklan') },
      { path: ':id/ubah', component: () => import('pages/Admin/iklan/UbahIklan') },
      { path: ':id/delete', component: () => import('pages/Admin/iklan/DeleteIklan') }
    ]
  },
  {
    path: '/produk',
    component: () => import('layouts/AuthLayout.vue'),
    children: [
      { path: 'saya', component: () => import('pages/Product/ListMyProduct.vue') },
      { path: 'hapus/:id', component: () => import('pages/Product/Delete.vue') },
      { path: 'publish/:id', component: () => import('pages/Product/Publish.vue') },
      { path: 'unpublish/:id', component: () => import('pages/Product/Unpublish.vue') },
      { path: 'tambah', component: () => import('pages/Product/Tambah.vue') },
      { path: 'create', component: () => import('pages/Product/Create') },
      { path: 'create/:step', component: () => import('pages/Product/Create') },
      { path: 'diperiksa', component: () => import('pages/Admin/daftarapprovalproduk.vue') },
      { path: 'diperiksa/:id', component: () => import('pages/Admin/detailapprovalproduk.vue') },
    ]
  },
  {
    path: '/jenis-produk',
    component: () => import('layouts/AuthLayout.vue'),
    children: [
      { path: 'tambah', component: () => import('pages/Admin/Tambahjp.vue') },
      { path: 'edit/:id', component: () => import('pages/JenisProduk/Edit.vue') },
      { path: 'hapus/:id', component: () => import('pages/JenisProduk/Hapus.vue') },
      { path: '', component: () => import('pages/JenisProduk/List.vue') },
    ]
  },
  {
    path: '/kategori',
    component: () => import('layouts/AuthLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Kategori/List.vue') },
      { path: 'tambah', component: () => import('pages/Kategori/Tambah.vue') },
      { path: 'delete/:id', component: () => import('pages/Kategori/Hapus.vue') },
      { path: 'edit/:id', component: () => import('pages/Kategori/Edit.vue') },
    ]
  },
  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
