import Pusher from "pusher-js";

var jarkonsPusher = new Pusher('eb205187ebf9fcc8e513', {
  authEndpoint: 'https://api.jarkons.com/broadcasting/auth',
  cluster: 'ap1'
});

var jarkonsChannel = jarkonsPusher.subscribe('jarkons-channel');

export { jarkonsPusher, jarkonsChannel }
