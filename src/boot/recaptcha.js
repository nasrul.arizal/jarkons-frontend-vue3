import Vue from 'vue'
import { VueReCaptcha } from 'vue-recaptcha-v3'

// For more options see below
Vue.use(VueReCaptcha, { siteKey: '6Le22asaAAAAAG8UAQl-yitpItCFp1YiRXivJzQs', loaderOptions: {
    autoHideBadge : true
  } })

