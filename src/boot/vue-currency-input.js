import VueCurrencyInput from 'vue-currency-input'

export default ({ Vue }) => {
  // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
  Vue.use(VueCurrencyInput)
}
