import Vue from 'vue'
import VueCryptojs from 'vue-cryptojs'

Vue.use(VueCryptojs)

Vue.prototype.$crypto = VueCryptojs

export { VueCryptojs }

