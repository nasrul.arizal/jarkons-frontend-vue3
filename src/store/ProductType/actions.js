import {axiosInstance} from "boot/axios";
import {Loading, Notify} from "quasar";

export function getSelectProductType(context) {
  axiosInstance.get("api/product-type/list/select")
    .then(response => {
      context.commit("setSelectProductType", response.data);
    })
}
