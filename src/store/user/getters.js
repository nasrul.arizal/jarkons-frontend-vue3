
export function getUser(state) {
  return state.user;
}

export function getAvatar(state) {
  return state.user.avatar;
}
