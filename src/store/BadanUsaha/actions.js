import {Loading, Notify} from "quasar";
import {axiosInstance} from "boot/axios";

export function getBadanUsahaById(storeBadanusaha, id) {
  Loading.show();
  let config = {
    url: "api/business/detail/" + id,
    method: "get",
    headers: {
      Authorization: "Bearer " + localStorage.getItem("token")
    }
  }

  axiosInstance(config)
    .then(response => {
      storeBadanusaha.commit("setBadanUsaha", response.data);
    })
    .catch(response => {
      let gagals = response.response;
      this.$router.push('/badan-usaha');
      Notify.create({
        position: "top",
        message: gagals.data.error,
        color: "red",
      })
    })
    .then(response => {
      Loading.hide();
    })
}

export function gagal(data) {
  this.$router.push('/badan-usaha');
  Notify.create({
    position: "top",
    message: gagals.data.error,
    color: "red",
  })
}

export function deleteBadanUsaha(storeBadanUsaha, id) {
  Loading.show();
  let config = {
    url: "api/business/delete/" + id,
    method: "post",
    headers: {
      Authorization: "Bearer " + localStorage.getItem("token")
    }
  }

  axiosInstance(config)
    .then(sukses => {
      this.$router.push('/badan-usaha');
      Notify.create({
        position: "top",
        message: "Berhasil Hapus Badan Usaha",
        color: "green",
      })
    })

    .catch(gagal => {
      let gagals = gagal.response;
      this.$router.push('/badan-usaha');
      Notify.create({
        position: "top",
        message: gagals.data.error,
        color: "red",
      })
    })

    .then(() => {
      Loading.hide();
    })
}


export function addBadanUsaha(storeBadanUsaha, id= 0) {
  let data  = storeBadanUsaha.getters["getBadanUsaha"]
  let config = {
    url: "api/business/create",
    method: "post",
    headers: {
      Authorization: "Bearer " + localStorage.getItem("token")
    },
    data: data
  }

  Loading.show();
  axiosInstance(config)
    .then(response => {
      this.$router.push('/badan-usaha');
      Notify.create({
        position: "top",
        message: "Berhasil Update Data",
        color: "green",
      })
    })
    .catch(response => {
      Notify.create({
        position: "top",
        message: response.data.error,
        color: "red",
      })
    })
    .then(response => {
      Loading.hide();
    })
}
