import {axiosInstance} from "boot/axios";
import {Loading, Notify} from "quasar";

export function deleteKategori(storeKategori, id) {
  Loading.show();
  let config = {
    url: "api/category/delete/" + id,
    method: "post",
    headers: {
      Authorization: "Bearer " + localStorage.getItem("token")
    }
  }
  axiosInstance(config)
    .then(sukses => {
      console.log("sukses", sukses);

      this.$router.push('/kategori');
      Notify.create({
        position: "top",
        message: "Berhasil Hapus Kategori",
        color: "green",
      })
      console.log("gagal", gagals);
    })
    .catch(gagal => {
      let gagals = gagal.response;
      this.$router.push('/kategori');
      Notify.create({
        position: "top",
        message: gagals.data.error,
        color: "red",
      })
      console.log("gagal", gagals);
    })
    .then(() => {
      Loading.hide();
    })

}

export function getSelectkategori(storeKategori) {
  axiosInstance.get("api/category/list-select")
    .then(response => {
      storeKategori.commit("setSelectKategori", response.data)
    })
}

export function getKategoriByTitle() {

}
