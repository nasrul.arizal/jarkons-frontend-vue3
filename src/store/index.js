import Vue from 'vue'
import Vuex from 'vuex'

// import example from './module-example'

import user from './user'
import kategori from './Kategori'
import badanusaha from './BadanUsaha'
import ProductType from './ProductType'
import product from "./product";
import lokasi from "./Lokasi";

Vue.use(Vuex)

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Store instance.
 */

export default function (/* { ssrContext } */) {
  const Store = new Vuex.Store({
    modules: {
      user,
      kategori,
      badanusaha,
      ProductType,
      product,
      lokasi
    },

    strict: false
  })

  if (process.env.DEV && module.hot) {
    module.hot.accept(['./user'], () => {
      const newShowcase = require('./user').default
      Store.hotUpdate({ modules: { newShowcase } })
    })

    module.hot.accept(['./product'], () => {
      const newShowcase = require('./product').default
      Store.hotUpdate({ modules: { newShowcase } })
    })

    module.hot.accept(['./Kategori'], () => {
      const newShowcase = require('./Kategori').default
      Store.hotUpdate({ modules: { newShowcase } })
    })

    module.hot.accept(['./BadanUsaha'], () => {
      const newBadanUsaha = require('./BadanUsaha').default
      Store.hotUpdate({ modules: { newBadanUsaha } })
    })
  }

  return Store
}
