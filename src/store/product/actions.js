import {axiosInstance} from "boot/axios";
import {Loading, Notify} from "quasar";

export function getProductDraft(context) {
  axiosInstance.post("api/product/draft")
    .then(response => {
      context.state.draft = response.data;
    })
}

export function addProductImage(context, val) {
  let data = new FormData();
  data.append("main_image", val.main_image)
  for (let gambar in val.image) {
    data.append("image[]", val.image[gambar]);
  }
  axiosInstance.post("api/product/image", data)
}

export function getDetailProduct(context, val) {
  Loading.show()
  axiosInstance.get("api/front/product/detail/" + val)
    .then(data => {
      Loading.hide();
      context.state.product = data.data
    })
}

export function getProductHome(context, val) {
  Loading.show()
  axiosInstance.get(`api/product/list/${val.flag}/${val.location}`)
    .then(data => {
      Loading.hide();
      context.commit("setProductHome", data.data.product.data)
    })
}

export function approveProduct(context, val) {
  Loading.show();
  let formData = new FormData();

  formData.append("id_product", val.id);
  formData.append("desc", val.desc);
  formData.append("status", val.status);

  axiosInstance.post("/api/product/approve-produk", formData)
    .then(sukses => {
      console.log("approve", sukses)
      Loading.hide()
      Notify.create({
        message: "Berhasil Approve Product",
      });
    })

    .catch(gagal => {
      Loading.hide()
    })
}

export function rejectProduct(context, val) {
  Loading.show();
  let formData = new FormData();

  formData.append("id_product", val.id);
  formData.append("desc", val.desc);
  formData.append("status", val.status);

  axiosInstance.post("/api/product/approve-produk", formData)
    .then(sukses => {
      console.log("approve", sukses)
      Loading.hide()
      Notify.create({
        message: "Produk Ditolak",
      });
    })

    .catch(gagal => {
      Loading.hide()
    })
}

export function unpublishProduct(context, val) {
  Loading.show();
  let formData = new FormData();

  formData.append("id_product", val.id);

  axiosInstance.post("/api/product/unpublish-produk", formData)
    .then(sukses => {
      console.log("approve", sukses)
      Loading.hide()
      Notify.create({
        message: "Produk Ditolak",
      });
    })

    .catch(gagal => {
      Loading.hide()
    })
}

export function publishProduct(context, val) {
  Loading.show();
  let formData = new FormData();

  formData.append("id_product", val.id);

  axiosInstance.post("/api/product/publish-produk", formData)
    .then(sukses => {
      console.log("approve", sukses)
      Loading.hide()
      Notify.create({
        message: "Produk Dipublikasi",
      });
    })

    .catch(gagal => {
      Loading.hide()
    })
}

export function getProductByKategori(context, val) {
  Loading.show()

  axiosInstance.get("api/product/category/" + val)
    .then(response => {
      Loading.hide();
      context.state.productListCategory = response.data;
      context.state.categoryActice = val;
    })

    .catch(error => {
      Loading.hide();
      context.state.productListCategory = "error";
    })
}
